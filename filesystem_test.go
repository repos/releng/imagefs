package imagefs

import (
	"context"
	"io"
	"io/fs"
	"testing"

	"github.com/containers/image/v5/oci/archive"
	blobinfocache "github.com/containers/image/v5/pkg/blobinfocache/memory"
	"github.com/containers/image/v5/types"
	"github.com/stretchr/testify/require"
)

func TestOpen(t *testing.T) {
	fs, assert := loadFixture(t, "basic")
	defer fs.Close()

	t.Run("can read file contents", func(t *testing.T) {
		assert.FileContentEqual("/foo", "foo\n")
	})

	t.Run("can read the same file twice", func(t *testing.T) {
		for i := 0; i < 2; i++ {
			assert.FileContentEqual("/foo", "foo\n")
		}
	})

	t.Run("reads from the outter most layer", func(t *testing.T) {
		assert.FileContentEqual("/bar", "bar2\n")
	})

	t.Run("resolves symlinks across layers", func(t *testing.T) {
		t.Skip("TODO")
		//assert.FileContentEqual("/foo-link", "foo\n")
	})
}

func loadFixture(t *testing.T, name string) (FS, *Assertions) {
	t.Helper()

	ref, err := archive.ParseReference("fixtures/basic.oci.tar")

	require.NoError(t, err)

	sys := &types.SystemContext{
		OSChoice:           "linux",
		ArchitectureChoice: "amd64",
	}

	fs := New(
		context.Background(),
		ref,
		sys,
		blobinfocache.New(),
	)

	return fs, NewAssertions(t, fs)
}

type Assertions struct {
	*require.Assertions

	t  *testing.T
	fs fs.FS
}

func NewAssertions(t *testing.T, fs fs.FS) *Assertions {
	t.Helper()

	return &Assertions{
		Assertions: require.New(t),
		t:          t,
		fs:         fs,
	}
}

func (assert *Assertions) FileContentEqual(path string, expected string) {
	file, err := assert.fs.Open(path)
	assert.NoError(err)

	actual, err := io.ReadAll(file)
	assert.NoError(err)

	assert.Equal(expected, string(actual))
}
