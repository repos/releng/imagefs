ImageFS provides access to an OCI container image filesystem using Go's
[FS](https://pkg.go.dev/io/fs#FS) interface. It was implemented primarily for
testing the [Blubber](https://gitlab.wikimedia.org/repos/releng/blubber)
BuildKit frontend.

It currently supports the following layer media types.

 - `application/vnd.oci.image.layer.v1.tar`
 - `application/vnd.oci.image.layer.v1.tar+gzip`
 - `application/vnd.oci.image.layer.nondistributable.v1.tar`
 - `application/vnd.oci.image.layer.nondistributable.v1.tar+gzip`

## Examples

### Read files from an OCI archive
```go
import (
  "context"
  
  "github.com/containers/image/v5/oci/archive"
  memoryblobcache "github.com/containers/image/v5/pkg/blobinfocache/memory"
  "github.com/containers/image/v5/types"
  imagefs "gitlab.wikimedia.org/repos/releng/imagefs"
)

ref, _ := archive.ParseReference("/some/oci/archive.tar")

sys := &types.SystemContext{
  OSChoice:           "linux",
  ArchitectureChoice: "amd64",
}

fs := imagefs.New(context.TODO(), ref, sys, memoryblobcache.New())

// Be sure to call Close() to remove temp files
defer fs.Close()

fs.Open("/some/file/from/the/image")
```
