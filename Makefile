BUILDX = docker buildx build -f .pipeline/blubber.yaml --platform linux/amd64

.PHONY: test clean

test: fixtures
	go test -test.v .

clean: clean-fixtures
clean-fixtures:
	rm fixtures/*.tar || true

fixtures: fixtures/basic.oci.tar
fixtures/basic.oci.tar:
	$(BUILDX) --target fixture-basic --output type=oci,tar=true,dest=./fixtures/basic.oci.tar .
